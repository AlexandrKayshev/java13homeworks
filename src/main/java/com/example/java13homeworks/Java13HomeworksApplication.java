package com.example.java13homeworks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.example.java13homeworks.dbexample.MyDBApplicationContext;
import com.example.java13homeworks.dbexample.dao.BookDAO;
import com.example.java13homeworks.dbexample.dao.UserDAO;
import com.example.java13homeworks.dbexample.model.User;

import java.sql.SQLException;
import java.time.LocalDate;

@SpringBootApplication
public class Java13HomeworksApplication implements CommandLineRunner{
	private BookDAO bookDAO;
	private UserDAO userDAO;

	@Autowired
	public void setBookDAO(BookDAO bookDAO) {
		this.bookDAO = bookDAO;

	}
	@Autowired
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	public static void main(String[] args) {
		SpringApplication.run(Java13HomeworksApplication.class, args);

	}

	@Override
	public void run(String... args) throws SQLException {

		System.out.println(bookDAO.findBookById(2));

		userDAO.addUserId(new User(1, "Кайшев", "Александр", LocalDate.of(1998,1,25),
				"8-800-555-25-35", "kay0398@mail.ru", "Берсерк, Волейбол, Необъятный океан"));
		userDAO.getEm("kay0398@mail.ru");
	}
}
