package com.example.java13homeworks.dbexample.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class User {
    private Integer userId;
    private String userSurname;
    private String userName;
    private LocalDate userDate;
    private String userPhone;
    private String userEmail;
    private String userListOfBooks;
}
